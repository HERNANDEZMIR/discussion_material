#include <stdio.h>
#include <string.h>

//check out https://www.w3schools.com/c/c_strings_functions.php
//and https://www.scaler.com/topics/c/string-functions-in-c/
//for a good listing of basic string processing functions

int main(int argc, char * argv[]) {
	char * st1 = "Hello World";
	char * st2 = "Hello World";
	char * st3 = "World";
	char st4[6];

	printf("Length of string '%s': %ld\n", st1, strlen(st1));

	printf("Are st1(%s) and st2(%s) the same? %d\n",st1,st2,strcmp(st1,st2));
	printf("Are st1(%s) and st3(%s) the same? %d\n",st1,st3,strcmp(st1,st3));
	printf("Are st3(%s) and st1(%s) the same? %d\n",st3,st1,strcmp(st3,st1));

	printf("'%s' before strcat() ...",st4);
	strcat(st4,st3);
	printf(" '%s' after\n",st4);

	printf("First occurance of 'o' in st1(%s) is at '%s'\n",st1,strchr(st1,'o'));
	printf("How about 'z' in st1(%s) is at '%s'\n",st1,strchr(st1,'z'));

	printf("What about 'Wor' in st1(%s) is at '%s'\n",st1,strstr(st1,"Wor"));
	printf("What about 'frog' in st1(%s) is at '%s'\n",st1,strstr(st1,"frog"));

	return 0;
}
